The goal is to capture changes in the v1k SLA colours (red to blue and vice versa) and then to output an audio alert. I think even the v504 SLA change of colour can be monitored concurrently too.
Apply this work to my VX Chart trading of the e-minis S&P.
Adapted from my other [project](https://gitlab.com/yeo.laurence/pyautogui-screenshot)

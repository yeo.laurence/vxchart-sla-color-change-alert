import numpy as np
import cv2 as cv
import os
import pyautogui
from time import time
import win32gui, win32con, win32ui
from PIL import ImageGrab
from playsound import playsound


'''def list_window_names():
    def winEnumHandler(hwnd, ctx):
        if win32gui.IsWindowVisible(hwnd):
            print(hex(hwnd), win32gui.GetWindowText(hwnd))
    win32gui.EnumWindows(winEnumHandler, None)
list_window_names()'''

def window_capture():

    w = 1800 # set this
    h = 1920 # set this

    hwnd = win32gui.FindWindow(None, '[ES] VXCharts - Build 1.0.7937.0')
    wDC = win32gui.GetWindowDC(hwnd)
    dcObj=win32ui.CreateDCFromHandle(wDC)
    cDC=dcObj.CreateCompatibleDC()
    dataBitMap = win32ui.CreateBitmap()
    dataBitMap.CreateCompatibleBitmap(dcObj, w, h)
    cDC.SelectObject(dataBitMap)
    cDC.BitBlt((0,0),(w, h) , dcObj, (0,0), win32con.SRCCOPY)

    # save screenshot
    signedIntsArray = dataBitMap.GetBitmapBits(True)
    img = np.fromstring(signedIntsArray, dtype='uint8')
    img.shape = (h, w, 4)
    img = ImageGrab.grab(bbox=(805, 830, 940, 900))
    img = np.array(img)
    img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
    print('a',img[10,10])

    # Free Resources
    dcObj.DeleteDC()
    cDC.DeleteDC()
    win32gui.ReleaseDC(hwnd, wDC)
    win32gui.DeleteObject(dataBitMap.GetHandle())

    return img

# prev_pix will  be initialised to BGR value seen now before the start of the following pyautogui 'while' loop below
# prev_pix will be compared to the curr_pix at every 'while' loop iteration
# [10,10] points to coordinate of return value 'img' by calling the 'window_display' definition
# Refer to the above 'window_display()' definition under 'def window_capture()'
prev_pix = window_capture()[10,10]

# helps programmers to test whether 'prev_pix' has been initialised; as stated right above
print('pp',prev_pix)

# 'loop_time" will first be initialised before the start of the following pyautogui 'while' loop below
# 'loop_time" will be compared at every 'while' loop iteration in order to calculate duration and frames per second
loop_time = time()

# start of the pyautogui window display 'while' loop; making frames at certain frames per second into a video
while(True):

    curr_pix = window_capture()[10, 10]

    # 'if' condition checks change in color of pixel at [10,10] position of screen; if colors changes, bell sound rings
    # 'curr_pix' and 'prev_pix' are arrays, and hence they need special comparison of BGR values
    # '(curr_pix == prev_pix).all()' returns BOOLEAN format
    # if both array variables are different, meaning '(curr_pix == prev_pix).all()' returns 'False'
    # hence, 'False' would be same as the 'False', hence making the IF condition TRUE, then execute 'if' loop
    if (curr_pix == prev_pix).all() == False:
        print('Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!')
        print('Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!Ring Ring!')
        playsound(r'C:\Users\zwyeo\PycharmProjects\Giraffe\RoadRunner.mp3')

    # 'prev_prix' will be used in the next 'while' iteration for comparison to assess whether colour (BGR) has changed
    prev_pix = curr_pix

    # Display the resulting frame
    screenshot = window_capture()
    cv.imshow('Computer Vision', screenshot)

    # displays frames per second; just for performance monitoring purposes
    print('FPS {}'.format(1/(time()-loop_time)))

    # 'loop_time' will be used in the next iteration for comparison to calculate FPS in the next 'while' iteration
    loop_time = time()

    # destroy window command to end pyautogui display operations by entering 'q' on keyboard
    if cv.waitKey(1) == ord('q'):
        cv.destroyAllWindows()
        break

# When everything done, release the capture
window_capture()
print('Done!')
playsound(r'C:\Users\zwyeo\PycharmProjects\Giraffe\allfolks.mp3')